﻿using System;
using System.Windows.Input;

namespace antiloma.Common
{
  public class CommandHandler:ICommand
	{
		private Action<object> _action;
    private Action<object[]> _actionWithParams;

		private bool _canExecute;

    public CommandHandler(Action<object> action)
    {
      _action = action;
      _canExecute = true;
    }
    public CommandHandler(Action<object[]> action)
    {
      _actionWithParams = action;
      _canExecute = true;
    }

		public bool CanExecute(object parameter)
		{
			return _canExecute;
		}

		public event EventHandler CanExecuteChanged;

		public void OnCanExecuteChanged()
		{
			CanExecuteChanged(this, EventArgs.Empty);
		}

		public void Execute(object obj)
		{
      if (obj is Array)
        _actionWithParams((object[])obj);
      else
        _action(obj);

		}
		
	}
}
