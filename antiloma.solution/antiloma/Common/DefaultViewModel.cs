﻿using System;
using System.ComponentModel;
using System.Linq.Expressions;

namespace antiloma.Common
{
  public abstract class DefaultViewModel : INotifyPropertyChanged
  {

    public event PropertyChangedEventHandler PropertyChanged;

    protected virtual void RaisePropertyChanged(string property)
    {
      if (PropertyChanged != null)
        PropertyChanged(this, new PropertyChangedEventArgs(property));

    }
    protected void RaisePropertyChanged(Expression<Func<string>> expression)
    {
      if (PropertyChanged != null)
        PropertyChanged(this, new PropertyChangedEventArgs(expression.Name));

    }
  }
}
