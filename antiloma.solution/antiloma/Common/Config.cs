﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace antiloma.Common
{
  static class Config
  {
    public static int BeginSecond { get; private set; }
    public static int EndSecond { get; private set; }
    public static void Load()
    {
      var config = ConfigurationManager.OpenExeConfiguration(System.Reflection.Assembly.GetExecutingAssembly().Location);
      BeginSecond=Convert.ToInt32(config.AppSettings.Settings["BeginSecond"].Value);
      EndSecond = Convert.ToInt32(config.AppSettings.Settings["EndSecond"].Value);

    }
  }
}
