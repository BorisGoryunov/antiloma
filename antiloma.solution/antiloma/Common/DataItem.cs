﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace antiloma.Common
{
  public sealed class DataItem:DefaultViewModel
  {
    private int _secondsToStart;
    private string _header;
    private readonly DispatcherTimer _timer = new DispatcherTimer();
    public DataItem(int value)
    {
      Value = value;
      Random rnd = new Random();
      SecondsToStart = rnd.Next(Config.BeginSecond, Config.EndSecond);
      SetHeader(SecondsToStart);
      _timer.Tick += TimerTick;
      _timer.Interval = new TimeSpan(0, 0, 1);
      _timer.Start();
    }
    void Convert()
    {
      try
      {
        Header = string.Concat(Value, "=", Value.ToBinary(), ", ", Value.ToHex());
      }
      catch(Exception exc)
      {
        Header = exc.Message;
      }
      
    }


    private void TimerTick(object sender, EventArgs e)
    {
      if (SecondsToStart <= 0)
      {
        _timer.Stop();
        Task task = new Task(Convert);
        task.Start();
      }
      else
      {
        SecondsToStart -= 1;
        SetHeader(SecondsToStart);
      }

    }

    void SetHeader(int seconds)
    {
      Header = string.Concat(Value," будет посчитано через... ", seconds," сек.");
    }
    public int Value { get; private set; }
    public int SecondsToStart
    {
      get
      {
        return _secondsToStart;
      }

      private set
      {
        _secondsToStart = value;
      }
    }

    public string Header
    {
      get
      {
        return _header;
      }

      private set
      {
        _header = value;
        RaisePropertyChanged("Header");
      }
    }
  }
}
