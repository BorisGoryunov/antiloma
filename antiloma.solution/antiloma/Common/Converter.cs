﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

namespace antiloma.Common
{
  static class ConverterExtension
  {
    [DllImport("converter.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi)]
    private static extern int ToBinary(int value,ref object result);

    [DllImport("converter.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi)]
    private static extern int ToHex(int value, ref object result);

    public static string ToBinary(this int value)
    {
      object result = null ;
      ConverterExtension.ToBinary(value,ref result);
      return result.ToString();
    }
    public static string ToHex(this int value)
    {
      object result = null;
      ConverterExtension.ToHex(value, ref result);
      return result.ToString();
    }
  }
}
