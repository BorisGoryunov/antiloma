﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using antiloma.Common;
using System.Collections.ObjectModel;

namespace antiloma.ViewModel
{
  public sealed class MainViewModel:DefaultViewModel
  {
    private readonly CommandHandler _commandAddNumber;
    private readonly IList<DataItem> _itemsSource = new ObservableCollection<DataItem>();
    private int _value;
    public MainViewModel()
    {
      _commandAddNumber = new CommandHandler(AddNumber);
    }

    public CommandHandler CommandAddNumber
    {
      get
      {
        return _commandAddNumber;
      }
    }

    public IList<DataItem> ItemsSource
    {
      get
      {
        return _itemsSource;
      }
    }

    public int Value
    {
      get
      {
        return _value;
      }

      set
      {
        _value = value;
        RaisePropertyChanged("Value");
      }
    }

    void AddNumber(object obj)
    {
      Config.Load();
      var item = new DataItem(Value);
      _itemsSource.Add(item);
      Value = 0;
    }


  }
}
