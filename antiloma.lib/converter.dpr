library converter;

{ Important note about DLL memory management: ShareMem must be the
  first unit in your library's USES clause AND your project's (select
  Project-View Source) USES clause if your DLL exports any procedures or
  functions that pass strings as parameters or function results. This
  applies to all strings passed to and from your DLL--even those that
  are nested in records and classes. ShareMem is the interface unit to
  the BORLNDMM.DLL shared memory manager, which must be deployed along
  with your DLL. To avoid using BORLNDMM.DLL, pass string information
  using PChar or ShortString parameters. }

uses
  System.SysUtils,
  System.Classes,
  ActiveX ;
{$R *.res}
function ToBinary(AValue:Integer;var AResult:OleVariant):Integer;stdcall ;
var
   i:Integer ;
   bin: string;
begin
   bin := '';
   for i := 1 to 8 * SizeOf(AValue) do
   begin
     if Odd(AValue) then
      bin := '1' + bin
     else
       bin := '0' + bin;
     AValue := AValue shr 1;
   end;
   Delete(bin, 1, 8 * ((Pos('1', bin) - 1) div 8));
   AResult := bin;
   result := 1; // real code would have real error handling
end;
function ToHex(AValue:Integer;var AResult:OleVariant):Integer ;stdcall ;
begin
  AResult:=IntToHex(AValue,1);
  result:=1 ;
end;

exports
  ToBinary ,
  ToHex
  ;

begin

end.
